const superagent = require("superagent");
const expect = require("expect");
const config = require("../config");
const urlBase = "http://" + config.api.host + ":" + config.api.port + "/" + config.version;
const mongoose = require("mongoose");

describe("Tests of API Rest", function () {
   before(function() {
      mongoose.connect("mongodb://" + config.database.user + ":" + config.database.password + "@" + config.database.host + "/" + config.database.dbname, { useNewUrlParser: true });
      mongoose.connection.on('error', function () {
         console.log("Error to open database.");         
      });
      mongoose.connection.on('open', function() {
         console.log("Database opened.");
         let model = mongoose.model('Test', new mongoose.Schema({ dna: String, specie: String }, { collection: config.database.collection }));
         model.deleteMany(function (error) {
            if (error) console.log();
         });
      });
   });

   after(function() {
      mongoose.connection.close(false, function() {
         console.log("Database closed.");
      })
   });

   it("Doing request to endpoint not existing", function (done) {
      superagent.get(urlBase + "/notexisting")
      .send()
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(404);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Method GET not implemented to endpoint " + "/" + config.version + "/notexisting.");
         done();
      });
   });

   it("Doing request to endpoint /simian with incorrect field of the body", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ incorrectfield: ["AAAA","AAAA","AAAA","AAAA"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with less than four sequences", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AAT", "CGC", "CGT"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with item of array dna not string", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AATC", "CGCC", "CGTC", 1] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with item of array dna with length less than 4", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AATC", "CGCC", "CGTC", "AT"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with item of array dna with caracter different of A, T, C, and G", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AATC", "CGCC", "CGXX", "ATAT"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with length of item different of length of array", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AATC", "CGCC", "CGAT", "ATAT", "CGCG"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(400);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Bad Request");
         done();
      });
   });

   it("Doing request to endpoint /simian with sequence of simian founded in horizontal direction", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["AAAAT", "CGCAT", "CGTCG", "CGCGC", "ATTAC"] })
      .end(function (err, res) {
         expect(err).toBeNull();
         expect(res.status).toEqual(200);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("OK");
         done();
      });
   });

   it("Doing request to endpoint /simian with sequence of simian founded in vertical direction", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["TCAAT", "CGCAT", "CGTCG", "CGCGC", "AGTAC"] })
      .end(function (err, res) {
         expect(err).toBeNull();
         expect(res.status).toEqual(200);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("OK");
         done();
      });
   });

   it("Doing request to endpoint /simian with sequence of simian founded in main diagonal direction", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["TCAAT", "CGCAT", "CGTCG", "CGCGC", "ACTAC"] })
      .end(function (err, res) {
         expect(err).toBeNull();
         expect(res.status).toEqual(200);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("OK");
         done();
      });
   });

   it("Doing request to endpoint /simian with sequence of simian founded in secondary diagonal direction", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["TCAAT", "CGCAT", "CGACG", "CACGA", "ACTAC"] })
      .end(function (err, res) {
         expect(err).toBeNull();
         expect(res.status).toEqual(200);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("OK");
         done();
      });
   });

   it("Doing request to endpoint /simian with sequence of human", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["TCAAT", "CGCAT", "CGTCG", "CACGA", "ACTAC"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(403);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(1);
         expect(keysBody).toContain("message");
         expect(res.body.message).toEqual("Forbidden");
         done();
      });
   });

   it("Doing request to endpoint /simian with a repeated sequence", function (done) {
      superagent.post(urlBase + "/simian")
      .send({ dna: ["TCAAT", "CGCAT", "CGTCG", "CACGA", "ACTAC"] })
      .end(function (err, res) {
         expect(err).not.toBeNull();
         expect(res.status).toEqual(500);
         done();
      });
   });

   it("Getting statistics", function (done) {
      superagent.get(urlBase + "/stats")
      .send()
      .end(function (err, res) {
         expect(err).toEqual(null);
         expect(res.status).toEqual(200);
         var keysBody = Object.keys(res.body);
         expect(keysBody.length).toEqual(3);
         expect(keysBody).toContain("count_simian_dna", "count_human_dna", "ratio"); 
         expect(res.body.count_simian_dna).toBeGreaterThanOrEqual(0);
         expect(res.body.count_human_dna).toBeGreaterThanOrEqual(0);
         expect(res.body.ratio).toBeGreaterThanOrEqual(0);
         done();
      });
   });
});
