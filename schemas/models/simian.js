module.exports = {
   name: "Simian",
   schema: {
      dna: String,
      specie: String
   }
};