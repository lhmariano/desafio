const joi = require("joi");

module.exports = {
   schema: joi.object().keys({
      dna: joi.array().min(4).items(joi.string().min(4).regex(/^[ATCG]+$/))
   })
};