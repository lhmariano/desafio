// Requires needed modules
const express = require("express");  
const bodyParser = require("body-parser");
const config = require("./config");
const lib = require("./lib");

// Instantiates the server
const app = express();

// Setup middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use((req, res, next) => {
   let results = lib.schemaValidator.validateRequest(req);

   if (results.valid) {
      return next();
   }

   res.status(400).json({ message:"Bad Request"});
});

app.use("/" + config.version, lib.router.setupRoutes());

app.use (function (req, res) {
   res.status(404).json({ message:"Method " + req.method + " not implemented to endpoint " + req.path + "."});
});

// Starts the server
const server = app.listen(config.api.port, function () {
   lib.logger.info("Server started successfully ...");
   lib.logger.info("Running in " + config.api.host + ":" + config.api.port + " ...");
   lib.db.connect(function (error) {
      if (error) {
         lib.logger.error("Error trying to connect to database: ", error);
         process.exit(0);
      } else {
         lib.logger.info("Database service started successfully ...");
      }
   });
});

// Graceful shutdown
process.on('SIGTERM', function () {
   lib.logger.info('SIGTERM signal received.');
   lib.logger.info('Closing the server.');
   server.close(function () {
      lib.logger.info('Server closed.');
      lib.db.close(function () {
         lib.logger.info('MongoDb connection closed.');
         process.exit(0);
      });
   });
 });
 