# Simian


This project solves the challenge posed by Mercado Livre to develop a system that detects if a DNA sequence belongs to a human or an simian.


## The Challange

Original text: "Em um futuro distante, na cadeia de evolução, os símios e os humanos estão cada vez mais próximos. Por esse motivo ficou muito difícil distinguir quem é humano e quem é símio.

Você é um cientista contratado para desenvolver um projeto que detecta se uma sequência de DNA pertence a um humano ou a um símio.

Para isso, você precisar desenvolver um programa, com um método ou função com a seguinte assinatura (em uma das seguintes linguagens: (Java / Golang / Javascript (Node) / Python)

boolean isSimian (String[] dna) // Exemplo em Java

Você receberá como parâmetro um array de Strings que representam cada linha de uma tabela quadrada de (NxN) com a sequência de DNA.

As letras da String só podem ser: (A, T, C, G), que representa cada base nitrogenada do DNA. Você saberá se um DNA pertence a um símio, se encontrar uma ou mais sequências de quatro letras iguais nas direções horizontais, verticais ou nas diagonais.

Exemplo (Símio):

String [] dna = {"CTGAGA", "CTGAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG"};

Nesse caso, a chamada para a função isSimian(String[] dna) deve retornar "true".

Com bases nessas informações, desenvolva o algoritmo da maneira mais eficiente possível de acordo com os desafios abaixo:

Desafios

* Nível 1:

Desenvolva um método ou função que esteja de acordo com a assinatura proposta isSimian(String[] dna), que seja capaz de identificar corretamente símios.

* Nível 2:

Crie uma API REST que disponibiliza um endpoint "/simian". Esse serviço recebe uma sequência de DNA através de um HTTP POST com um JSON que contém o seguinte formato, exemplo:

POST → /simian
{
"dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}

Caso o DNA seja identificado como um símio, você deve retornar um HTTP 200-OK, caso contrário um HTTP 403-FORBIDDEN

+ Nível 3:

Crie um banco de dados, que armazena os DNAs verificados pela API. Esse banco deve garantir a unicidade, ou seja, apenas 1 registro por DNA.

Disponibilizar um serviço extra "/stats" que responde um HTTP GET. A resposta deve ser um Json que
retorna as estatísticas de verificações de DNA, onde deve informar a quantidade de DNA’s símios,
quantidade de DNA’s humanos, e a proporção de símios para a população humana.

Segue exemplo da resposta:

{"count_mutant_dna": 40, "count_human_dna": 100: "ratio": 0.4}"


# Observations


The developed project was implemented in Javascript (Node), using the NoSQL MongoDB database.

The letters of strings can be only A, C, T e G (uppercase).


# API


It was implemented a API REST, with two endpoints: /simian e /stats.

To run the api, execute the comand 'npm start' (without the quotation marks).


# Database


The database used was the NoSQL MongoDB. To implement the solution was created the bank 'evolution' and the collection 'species', whose scheme is as follows: {dna: string, specie: string}. For the tests, the database 'dbtest' and the 'coltest' collection were created, with the same scheme mentioned above.


# Creation of Database


To create these banks simply run the following commands in MongoDB:

* Bank of Implementatiom


```
use evolution
db.createCollection("species")
db.species.createIndex({ "dna": 1 }, { "unique": true })
db.createUser({ "user":"mercadolivre", "pwd":"mercadolivre", "roles": [{ "role":"readWrite", "db":"evolution"}]})
```

* Bank of test


```
use dbtest
db.createCollection("coltest")
db.coltest.createIndex({ "dna": 1 }, { "unique": true })
db.createUser({ "user":"test", "pwd":"test", "roles": [{ "role":"readWrite", "db":"dbtest"}]})
```

Para usar autenticação no MongoDB basta habilitá-la no arquivo /etc/mongod.conf, adicionando o seguinte trecho:

To use authentication in MongoDB simply enable it in the file /etc/mongo.conf, adding the following:


```
security:
  authorization: enabled
```

## Tests


To run the tests, the mongodb database must be running and with the test database, collection, and user created.

To test, the api must be running with the .env of test. To do this, simply edit .env from the project root, leaving the .env.test equal to / config.

To run the tests, simply enter the 'npm test' command (without the quotation marks).

The mongodb database must be running and with the test database, collection, and user created.
