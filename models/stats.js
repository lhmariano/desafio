const requireDir = require("require-dir");
const schemaModels = requireDir("../schemas/models");
const config = require("../config");
const mongoose = require("mongoose");

module.exports = function () {
   return(mongoose.model(schemaModels["stats"].name,new mongoose.Schema(schemaModels["stats"].schema, {collection: config.database.collection})));
};