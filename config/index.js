const dotenv = require("dotenv").config();

module.exports = {
   "version": process.env.VERSION,
   "env": process.env.ENV,
   "api": {
      "host": process.env.API_HOST,
      "port": process.env.API_PORT
   },
   "database": {
      "host": process.env.DB_HOST,
      "port": process.env.DB_PORT,
      "dbname": process.env.DB_DBNAME,
      "collection": process.env.DB_COLLECTION,
      "user": process.env.DB_USER,
      "password": process.env.DB_PASSWORD
   }
};