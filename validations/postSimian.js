module.exports = {
   validate: function (body) {
      let res = { "valid": true };
      let dna = body.dna;
      let n = dna.length;
      let i = 0;

      while (res.valid && i < n) {
         // The length of each string into the array must be equal to the length of array.
         if (dna[i].length != n) {
            res = { "valid": false };
         }
         i++;
      }

      return (res);
   }
};