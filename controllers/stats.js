const db = require("../lib/db");
const logger = require("../lib/logger");

function getStats (req, res) {
   let Stats = db.models.stats;
   let aggregatorOpts = [{
      $group: {
         _id: "$specie",
         count: { $sum: 1}
      }
   }];

   Stats.aggregate(aggregatorOpts).exec().then(function (aggregate) {
      let countSimian = 0;
      let countHuman = 0;
      
      if (aggregate[0]._id == "simian") {
         countSimian = aggregate[0].count;
         countHuman = aggregate[1].count;
      } else {
         countSimian = aggregate[1].count;
         countHuman = aggregate[0].count;
      }

      res.status(200).json({"count_simian_dna": countSimian, "count_human_dna": countHuman, "ratio": countSimian / countHuman});
   }).catch(function (error) {
      logger.error(error.message);
      res.status(500).json({ message: error.message});
   });
}

module.exports = {
   getStats: getStats
};