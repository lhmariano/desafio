const species = require("../lib/species");
const db = require("../lib/db");
const logger = require("../lib/logger");

function isSimian (req, res) {
   let result = species.isSimian(req.body.dna);
   let Simian = db.models.simian;
   let item = { dna: req.body.dna.join(""), specie: result ? "simian" : "human" };
   let data = new Simian(item);

   data.save().then(function () {
      if (result) {
         res.status(200).json({ message: "OK" });
      } else {
         res.status(403).json({ message: "Forbidden" });
      }
   }).catch(function (error) {
      logger.error(error.message);
      res.status(500).json({ message: error.message});
   });
}

module.exports = {
   isSimian: isSimian
};
