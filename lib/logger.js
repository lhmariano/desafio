const config = require("../config");

const { createLogger, format, addColors, transports } = require('winston');
const { combine, timestamp, label, printf, colorize } = format;

const myFormat = printf(function (info) {
   return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const myCustomLevels = {
   levels: {
      error: 0,
      warn: 1,
      info: 2,
      debug: 3,
   },
   colors: {
      error: 'red',
      warn: 'yellow',
      info: 'blue',
      debug: 'violet'
   }
};

const logger = createLogger({
   format: combine(
      colorize(),
      label({ label: config.env }),
      timestamp(),
      myFormat
   ),
   levels: myCustomLevels.levels,
   transports: [new transports.Console()]
});

addColors(myCustomLevels);

module.exports = logger;