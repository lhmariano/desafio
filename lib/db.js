const mongoose = require("mongoose");
const config = require("../config");

module.exports = {
   connect: function connect (cb) {
      mongoose.connect("mongodb://" + config.database.user + ":" + config.database.password + "@" + config.database.host + "/" + config.database.dbname, { useNewUrlParser: true });
      this.connection = mongoose.connection;
      this.connection.on('error', cb);
      this.connection.once('open', cb);  
   },
   models: require("../models"),
   close: function (cb) {
      mongoose.connection.close(false, cb)
   }
};