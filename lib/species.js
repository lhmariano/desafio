const patterns = /A{4}|T{4}|C{4}|G{4}/; // /AAAA|TTTT|CCCC|GGGG/ é outra opção

function match (pattern, sequence) {
   return (pattern.test(sequence));
}

function searchInHorizontal (dna, n) {
   console.log("---Horizontal----");
   let foundPattern = false;
   let line = 0;

   while (!foundPattern && line < n) {
      console.log(dna[line]);
      console.log(match(patterns, dna[line]));

      if (match(patterns, dna[line])) {
         foundPattern = true;
      }
         
      line++;
   }

   return (foundPattern);
}

function searchInVertical (dna, n) {
   console.log("---Vertical---");
   let foundPattern = false;
   let column = 0;
   let sequence = "";

   while (!foundPattern && column < n) {
      for (let line = 0; line < n; line++) {
         sequence += dna[line][column];
      }

      console.log(sequence);
      console.log(match(patterns, sequence));

      if (match(patterns, sequence)) {
         foundPattern = true;
      }

      sequence = "";
      column++;
   }

   return (foundPattern);
}

function searchInMainDiagonal (dna, n) {
   console.log("---Main Diagonal---");
   let foundPattern = false;
   let i = n - 4;
   let sequence = "";

   while (!foundPattern && i > 0) {
      for (let line = i, column = 0; line < n; line++, column++) {
         sequence += dna[line][column];
      }
      console.log(sequence);
      console.log(match(patterns, sequence));

      if (match(patterns, sequence)) {
         foundPattern = true;
      } else {
         sequence = "";

         for (let line = 0, column = i; column < n; line++, column++) {
            sequence += dna[line][column];
         }

         console.log(sequence);
         console.log(match(patterns, sequence));

         if (match(patterns, sequence)) {
            foundPattern = true;
         }
      }

      sequence = "";
      i--;
   }

   if (!foundPattern) {
      for (let line = 0; line < n; line++) {
         sequence += dna[line][line];
      }

      console.log(sequence);
      console.log(match(patterns, sequence));

      if (match(patterns, sequence)) {
         foundPattern = true;
      }
   }

   return (foundPattern);
}

function searchInSecondaryDiagonal (dna, n) {
   console.log("---Secondary Diagonal---");
   let foundPattern = false;
   let i = 3;
   let sequence = "";

   while (!foundPattern && i < n - 1) {
      for (let line = 0, column = i; column >= 0; line++, column--) {
         sequence += dna[line][column];
      }
      console.log(sequence);
      console.log(match(patterns, sequence));

      if (match(patterns, sequence)) {
         foundPattern = true;
      } else {
         sequence = "";

         for (let line = n - i - 1, column = n - 1; line < n; line++, column--) {
            sequence += dna[line][column];
         }
         console.log(sequence);
         console.log(match(patterns, sequence));

         if (match(patterns, sequence)) {
            foundPattern = true;
         }
      }

      sequence = "";
      i++;
   }

   if (!foundPattern) {
      for (let line = 0, column = n - 1; line < n; line++, column--) {
         sequence += dna[line][column];
      }

      console.log(sequence);
      console.log(match(patterns, sequence));

      if (match(patterns, sequence)) {
         foundPattern = true;
      }
   }

   return (foundPattern);
}

function isSimian (dna) {
   let n = dna.length;
   
   return (searchInHorizontal(dna, n) || searchInVertical(dna, n) || searchInMainDiagonal(dna, n) || searchInSecondaryDiagonal(dna, n));
}

module.exports = {
   isSimian: isSimian
};
