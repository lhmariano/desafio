const express = require("express");
const router = express.Router();
const controllers = require("../controllers");

function setupRoutes () {
   router.post("/simian", controllers.simian.isSimian);
   router.get("/stats", controllers.stats.getStats);

   return (router);
}

module.exports = {
   setupRoutes: setupRoutes
};