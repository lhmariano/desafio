module.exports = {
   logger: require("./logger"),
   schemaValidator: require("./schemaValidator"),
   router: require("./router"),
   species: require("./species"),
   db: require("./db")
};