const joi = require("joi")
const requireDir = require("require-dir"); 
const schemasRequests = requireDir("../schemas/requests");
const validations = requireDir("../validations");
const utils = require("./utils");

function validate (req) {
   let res = { valid: true };
   let schemaName = req.method.toLowerCase() + utils.capitalize(req.path.split("/").pop());

   // If exists schema, validates JSON into body against schema.
   if (schemasRequests[schemaName]) {
      let result = joi.validate(req.body, schemasRequests[schemaName].schema);

      if (result.error) {
         res = { valid: false };
      }

      if (res.valid) {
         // If exists aditional validation, applies it against JSON into body. 
         if (validations[schemaName]) {
            res = validations[schemaName].validate(req.body);
         }
      }
   }

   return (res);
}

module.exports = {
   validateRequest: validate
}
